//
//  ViewController.swift
//  prototypingApp
//
//  Created by mazodirty on 20/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet weak var goldenBridgeImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: CustomButton!
    @IBOutlet weak var backgroundButton: UIView!
    
    @IBOutlet weak var containerDate: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var containerWeather: UIView!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var pencilButton: CustomButton!
    @IBOutlet weak var chatButton: CustomButton!
    @IBOutlet weak var alarmButton: CustomButton!
    
    var schedules = [Schedule]()
    var weatherItem: WeatherModel?
    
    struct IDS {
        static let scheduleCell = "ScheduleCell"
        static let scheduleCellWithImages = "ScheduleCellWithImages"
    }
    
    lazy var gradientLayer: CAGradientLayer = {
        let layerGradient = CAGradientLayer()
        let purple = UIColor(red: 89/255, green: 93/255, blue: 154/255, alpha: 1)
        layerGradient.colors = [
            purple.withAlphaComponent(1).cgColor,
            purple.withAlphaComponent(0.9).cgColor,
            purple.withAlphaComponent(0.8).cgColor,
            purple.withAlphaComponent(0.7).cgColor,
            purple.withAlphaComponent(0.6).cgColor
        ]
        
        layerGradient.frame = self.view.frame
        layerGradient.locations = [0.28, 0.45, 0.6, 0.7, 0.9]
        
        return layerGradient
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupGradient()
        setupBackgroundButton()
        setupData()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ScheduleCell.self, forCellReuseIdentifier: IDS.scheduleCell)
        tableView.register(ScheduleCell.self, forCellReuseIdentifier: IDS.scheduleCellWithImages)
        
        setupAnimations()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
         UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
            
            self.containerDate.transform = .identity
            self.containerWeather.transform = .identity
            
         }, completion: nil)
    }
    
    func setupAnimations() {
        containerDate.transform = CGAffineTransform(translationX: -containerDate.frame.width, y: 0)
        containerWeather.transform = CGAffineTransform(translationX: containerWeather.frame.width, y: 0)
    }
    
    func setupData() {
        var weatherList = Data.getData()
        weatherItem = weatherList[0]
        
        dayLabel.text = weatherItem?.dayName
        dateLabel.text = weatherItem?.longDate
        weatherLabel.text = weatherItem?.temperature
        cityLabel.text = weatherItem?.city
        if let image = weatherItem?.weatherIcon {
            weatherImage.image = UIImage(named: image)
        }
        
        //setup cell data
        Data.getSchedules(completion: { (schedules) in
            self.schedules = schedules
            self.tableView.reloadData()
            self.animateTableCells()
        })
        
    }
    
    func animateTableCells() {
        
        let cells = tableView.visibleCells
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: view.frame.width, y: 0)
        }
        
        var delay = 0.6
        
        for cell in cells {
            UIView.animate(withDuration: 0.3, delay: delay, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
                
                cell.transform = .identity
                
            }, completion: nil)
            
            delay += 0.2
        }
    }
    
    func setupBackgroundButton() {
        backgroundButton.layer.cornerRadius = 170
        closeMenu()
    }
    
    func setupGradient() {
        view.layer.insertSublayer(gradientLayer, at: 1)
    }
    
    
    //Animate addButton
    @IBAction func addButtonClicked(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            if self.addButton.transform == .identity {
                self.backgroundButton.transform = .identity
                self.addButton.transform = CGAffineTransform(rotationAngle: self.radiansToDegrees(radians: 45))
                self.addButton.backgroundColor = UIColor(white: 0.2, alpha: 0.25)
                
                self.bounceMenuButtons()
                
            }else {
                self.closeMenu()
                self.addButton.transform = .identity
                self.addButton.backgroundColor = UIColor(red: 242/255, green: 51/255, blue: 99/255, alpha: 1)
            }
            
        })
    }
    
    //Bounce the menu buttons
    func bounceMenuButtons() {
        UIView.animate(withDuration: 1, delay: 0.2, usingSpringWithDamping: 0.2, initialSpringVelocity: 0, options: [], animations: {
            
            self.pencilButton.transform = .identity
            self.chatButton.transform = .identity
            self.alarmButton.transform = .identity
            
        }, completion: nil)
    }
    
    func closeMenu() {
        backgroundButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        pencilButton.transform = CGAffineTransform(translationX: 0, y: 25)
        chatButton.transform = CGAffineTransform(translationX: 40, y: 30)
        alarmButton.transform = CGAffineTransform(translationX: 60, y: 0)
    }
    
    func radiansToDegrees(radians: CGFloat) -> CGFloat {
        return (radians * .pi) / 180
    }
    
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = self.view.frame
    }
    
}


//MARK: -> UITableViewDatasource
extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedules.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: IDS.scheduleCell, for: indexPath) as! ScheduleCell
        cell.schedule = schedules[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let schedule = schedules[indexPath.row]
        
        let width = self.view.frame.width - 20 - 50 - 20 - 8
        let size = CGSize(width: width, height: 1000)
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
        
        if let subtitle = schedule.subtitle {
            let calculateRect = NSString(string: subtitle).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            
            if let images = schedule.images, images.count > 0 {
                return calculateRect.height + 10 + 30 + 8 + 10 + 44
            }
            
            return calculateRect.height + 10 + 30 + 8 + 10
            
        }else {
            return 80
        }
    }
    
}


//ScheduleCell
class ScheduleCell: UITableViewCell {
    
    var schedule: Schedule? {
        didSet {
            guard let schedule = schedule else {
                return
            }
            
            hourLabel.text = schedule.hour
            kindLabel.text = schedule.kindHour
            titleLabel.text = schedule.title
            subtitle.text = schedule.subtitle
            
            if let images = schedule.images, images.count > 0 {
                
                stackViewBottomConstraint?.isActive = false
                
                fillStackViewImages()
                
                stackViewImagesTopConstraint?.isActive = true
                stackViewImagesLeftConstraint?.isActive = true
                stackViewImagesBottomConstraint?.isActive = true
                
                
            }else {
                stackViewBottomConstraint?.isActive = true
                stackViewImagesTopConstraint?.isActive = false
                stackViewImagesLeftConstraint?.isActive = false
                stackViewImagesBottomConstraint?.isActive = false
            }
            
        }
    }
    
    let hourLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        
        return label
    }()
    
    let kindLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.sizeToFit()
        label.setContentHuggingPriority(261, for: .vertical)
        
        return label
    }()
    
    let subtitle: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0.8, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.sizeToFit()
        
        return label
    }()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.titleLabel, self.subtitle])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    lazy var stackViewImages: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        backgroundColor = UIColor.clear
        separatorInset = UIEdgeInsets(top: 0, left: 92, bottom: 0, right: 0)
    }
    
    var stackViewBottomConstraint: NSLayoutConstraint?
    var stackViewImagesTopConstraint: NSLayoutConstraint?
    var stackViewImagesLeftConstraint: NSLayoutConstraint?
    var stackViewImagesBottomConstraint: NSLayoutConstraint?
    
    func setupViews() {
        addSubview(hourLabel)
        addSubview(kindLabel)
        addSubview(stackView)
        addSubview(stackViewImages)
        
        hourLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        hourLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        hourLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        hourLabel.setContentHuggingPriority(261, for: .horizontal)
        
        kindLabel.centerXAnchor.constraint(equalTo: hourLabel.centerXAnchor).isActive = true
        kindLabel.topAnchor.constraint(equalTo: hourLabel.bottomAnchor).isActive = true
        
        stackView.topAnchor.constraint(equalTo: hourLabel.topAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: hourLabel.rightAnchor, constant: 20).isActive = true
        stackViewBottomConstraint = stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        stackViewBottomConstraint?.isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        
        stackViewImagesTopConstraint = stackViewImages.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 4)
        stackViewImagesTopConstraint?.isActive = false
        stackViewImagesBottomConstraint = stackViewImages.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        stackViewImagesBottomConstraint?.isActive = false
        stackViewImagesLeftConstraint = stackViewImages.leftAnchor.constraint(equalTo: stackView.leftAnchor)
        stackViewImagesLeftConstraint?.isActive = false
        
    }
    
    func fillStackViewImages() {
        if let images = schedule?.images {
            
            for image in images {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
                imageView.image = image
                imageView.layer.cornerRadius = 18
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                imageView.translatesAutoresizingMaskIntoConstraints = false
                imageView.heightAnchor.constraint(equalToConstant: 36).isActive = true
                imageView.widthAnchor.constraint(equalToConstant: 36).isActive = true
                
                stackViewImages.addArrangedSubview(imageView)
            }
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}

















