//
//  WeatherModel.swift
//  prototypingApp
//
//  Created by mazodirty on 20/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class WeatherModel: NSObject {
    
    var dayName: String!
    var longDate: String!
    var temperature: String!
    var city: String!
    var weatherIcon: String!
    
    init(dayName: String, longDate: String, temperature: String, city: String, weatherIcon: String) {
        self.dayName = dayName
        self.longDate = longDate
        self.temperature = temperature
        self.city = city
        self.weatherIcon = weatherIcon
    }
    
}
