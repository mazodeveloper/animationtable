//
//  Schedule.swift
//  prototypingApp
//
//  Created by mazodirty on 21/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class Schedule: NSObject {
    
    var title: String!
    var subtitle: String?
    var hour: String!
    var kindHour: String!
    var images: [UIImage]?
    
    init(title: String, subtitle: String?, hour: String, kindHour: String, images: [UIImage]?) {
        self.title = title
        self.subtitle = subtitle
        self.hour = hour
        self.kindHour = kindHour
        self.images = images
    }
    
}












