//
//  Data.swift
//  prototypingApp
//
//  Created by mazodirty on 20/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class Data {
    
    class func getData() -> [WeatherModel] {
        var weatherList = [WeatherModel]()
        let weather1 = WeatherModel(dayName: "Thursday", longDate: "July 20, 2017", temperature: "27º", city: "San Francisco", weatherIcon: "sun")
        weatherList.append(weather1)
        
        return weatherList
    }
    
    static func getSchedules(completion: @escaping ([Schedule]) -> ()) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            var schedules = [Schedule]()
            var schedule = Schedule(title: "Finish home screen, more text my man Finish home screen Finish home screen Finish home screen Finish home screen Finish home screen Finish home screen", subtitle: "Movil app Finish home screen, more text my man Finish home screen Finish home screen Finish home screen Finish home screen Finish home screen Finish home screen jeeee cousin bro", hour: "08", kindHour: "AM", images: nil)
            schedules.append(schedule)
            schedule = Schedule(title: "Lunch break", subtitle: nil, hour: "11", kindHour: "AM", images: nil)
            schedules.append(schedule)
            schedule = Schedule(title: "Design stand up", subtitle: "Hangouts", hour: "02", kindHour: "PM", images: [#imageLiteral(resourceName: "picture1"), #imageLiteral(resourceName: "picture2"), #imageLiteral(resourceName: "picture3")])
            schedules.append(schedule)
            
            
            DispatchQueue.main.async {
                completion(schedules)
            }
            
        }
    }
    
}







